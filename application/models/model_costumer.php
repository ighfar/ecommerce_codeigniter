<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_costumer extends CI_Model {

public function get_shopping_history($user){

     $hasil = $this->db->select('i.*, SUM(o.qty * o.price) AS total')
                       ->from('invoices i, pelanggan u, orders o') 
                       ->where('u.nama_user', $user)
                       ->where('u.id = i.user_id')
                       ->where('o.invoice_id = i.id')
                       ->where('o.invoice_id')
                       ->get();
       
       if($hasil->num_rows>0){

        return $hasil->result();
       } else {

        return false;
       }
    }

		public function mark_invoice_confirmed($invoice_id, $jumlah){

      $ret = true;
      $invoice = $this->db->where('id', $invoice_id)->limit(1)->get('invoices');
      if ($invoice->num_rows == 0) {
        $ret = $ret && false;

      } else {
             $total = $this->db->select('SUM(qty * price) AS total')
                       ->where('invoice_id', $invoice_id)
                       ->get('orders');

            if($total->row()->total > $jumlah){
                     $ret = $ret && false;

            } else {

              $this->db->where('id', $invoice_id)->update('invoices', array('status' => 'confirmed' ));
            }

      }
      return $ret;

    }
}