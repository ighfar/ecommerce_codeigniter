<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_login extends CI_model {

	public function check_credential()
	{
		$nama_user = set_value('nama_user');
		$password = set_value('password');
			
		$hasil = $this->db->where('nama_user', $nama_user)
						  ->where('password', $password)
						  ->limit(1)
						  ->get('pelanggan');
		
		if($hasil->num_rows() > 0){
			return $hasil->row();
		} else {
			return array();
		}
	}


}