<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_kategori extends CI_Model {
	   var $tabel = 'kategori';

    function __construct() {
        parent::__construct();
    }

	public function all(){
		//query semua record di table products
		$hasil = $this->db->get('kategori');
		if($hasil->num_rows() > 0){
			return $hasil->result();
		} else {
			return array();
		}
	}
	
	public function find($no_kategori){
		//Query mencari record berdasarkan ID-nya
		return $this->db->get_where('kategori',array('no_kategori'=>$no_kategori))->row();
	}

					  
	 	public function getkategori($where=""){
		//Query mencari record berdasarkan ID-nya
			$data = $this->db->query('select * from kategori',$where);
		return $data;
	}	

						  
	 function get_insert($data){
       $this->db->insert($this->tabel, $data);
       return TRUE;
    }

	public function create($data_products){
		//Query INSERT INTO
		$this->db->insert('kategori', $data_products);
	}

	public function update($no_kategori, $data){
		//Query UPDATE FROM ... WHERE id=...
		$this->db->where('no_kategori', $no_kategori)
				 ->update('kategori', $data);
	}
	
	public function delete($no_kategori){
		//Query DELETE ... WHERE id=...
			$this->db->where('no_kategori', $no_kategori)
				 ->delete('kategori', $data);
	}
	
}