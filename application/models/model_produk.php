<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_produk extends CI_Model {
	   var $tabel = 'produk';

    function __construct() {
        parent::__construct();
    }


   public function search($keyword)
    {
        $this->db->like('nama_produk',$keyword);
        $query  =   $this->db->get('produk');
        return $query->result();
    }

	public function all(){
		//query semua record di table products
		$hasil = $this->db->get('produk');
		if($hasil->num_rows() > 0){
			return $hasil->result();
		} else {
			return array();
		}
	}
public function cariProduk($cari)
{
$cari=$this->db->query("select * from produk where nama_produk like '%$cari%'");
return $cari->result();
}
	
	public function find($id){
		//Query mencari record berdasarkan ID-nya
		$hasil = $this->db->where('id', $id)
						  ->limit(1)
						  ->get('produk');
		if($hasil->num_rows() > 0){
			return $hasil->row();
		} else {
			return array();
		}
	}
public function findget($no_kategori){
		//Query mencari record berdasarkan ID-nya
		$this->db->where('no_kategori',$no_kategori);
			return $this->db->get('produk')->result();
	}

			public function getkategori($where=""){
		//Query mencari record berdasarkan ID-nya
			$data = $this->db->query('select * from kategori',$where);
		return $data;
	}	

		public function getproduk($where=""){
		//Query mencari record berdasarkan ID-nya
			$data = $this->db->query('select * from produk',$where);
		return $data;
	}			  
	 function get_insert($data){
       $this->db->insert($this->tabel, $data);
       return TRUE;
    }

	public function create($data_products){
		//Query INSERT INTO
		$this->db->insert('produk', $data_products);
	}

	public function update($id, $data){
		//Query UPDATE FROM ... WHERE id=...
		$this->db->where('id', $id)
				 ->update('produk', $data);
	}
	
	public function delete($id){
		//Query DELETE ... WHERE id=...
		$this->db->where('id', $id)
				 ->delete('produk');
	}
	
}