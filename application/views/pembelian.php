<!DOCTYPE html>
<html>
<head>
	<title>eCommerce</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.transition.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/jquery-1.11.3.min.js"></script>
</head>
<body>

	 <?php $this->load->view('header');?>

    <?php $this->load->view('menu_user');?>
	<div class="content-product">
		<div class="container">
			<h2 class="header-title">
				DATA PEMBELIAN
			</h2><div class="product-detail">
		<div class="container">
			<div class="left">
	<div class="beli">
		<div class="container">
			
			<div class="content">
	
				<h2 class="nama">
					Alamat
				</h2>
				<?php if($this->session->userdata('nama_user')) { ?>
				<p class="text">
					<?=$this->session->userdata('alamat')?>
				</p><?php } ?>
		
			<h2 class="nama">
					Data Produk
				</h2>
				
				<img src="<?php echo base_url();?>elia/uploads/<?php echo $hasil->gambar;?>" class="avatar" />
				<h3 class="nama">
				<?php echo $hasil->nama_produk;?>
				</h3>	<form action="" method="post">
				<select option name="" class="select">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
					</select>
					
					<button type="submit" class="submit"><i class="fa fa-send-o"></i>Hitung</button>
				</form>
				<?php $hg = $hasil->harga; $jm = $hasil->jumlah; $total = $hg * $jm;?>

				<p class="text">
			<?php echo $total;?>
				</p>
						
			</div><!-- content -->
		</div><!-- container -->
	</div><!-- testimonial -->
	
				
							
				
			</div><!-- left -->
			<div class="right">
				
			
			</div><!-- right -->
		</div><!-- container -->
	</div><!-- product detail -->
	</div><!-- content product -->

	<div class="testimonial">
		<div class="container">
			<h2 class="header-title">
				OUR TESTIMONIAL
			</h2>
			<div class="content">
				<img src="images/produk1.jpg" class="avatar" />
				<h3 class="nama">
					ayolearn.com
				</h3>
				<p class="text">
					" Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit "
				</p>
			</div><!-- content -->
			<div class="content">
				<img src="images/produk3.jpg" class="avatar" />
				<h3 class="nama">
					ayolearn-anggarisky.com
				</h3>
				<p class="text">
					" Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit "
				</p>
			</div><!-- content -->
		</div><!-- container -->
	</div><!-- testimonial -->

	<div class="mega-footer">
		<div class="container">
			<div class="content">
				<h2>ABOUT US</h2>mmmmmmmmmmmmmmm
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat.
				</p>
			</div><!-- content -->
			<div class="content">
				<h2>SITEMAP</h2>
				<ul>
					<li><a href="#">Home</a></li>
					<li><a href="#">Downloads</a></li>
					<li><a href="#">Contact</a></li>
					<li><a href="#">Jobs</a></li>
				</ul>
			</div><!-- content -->
			<div class="content">
				<h2>GET IN TOUCH</h2>
				<p>Jl. Inkopad, Blok B11 No. 7, Bogor, Jawa Barat.</p>
				<p><i class="fa fa-phone"></i> (0251) 123456789</p>
				<p><i class="fa fa-envelope"></i> cs@ayolearn.com</p>
			</div><!-- content -->
		</div><!-- container -->
	</div><!-- mega footer -->

	<div class="small-footer">
		<div class="container">
			<p class="copyright">
				All Right Reserved Ayolearn.com 2015
			</p>
			<p class="link">
				<a href="#">Top</a>
			</p>
		</div><!-- container -->
	</div><!-- small footer -->

	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/main.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/wow.js"></script>


</body>
</html>