<!DOCTYPE html>
<html>
<head>
		<title>eCommerce</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.transition.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/jquery-1.11.3.min.js"></script>
</head>
<body>
<div class="navbar-small">
		<div class="container">
			<ul class="left">
				<li><a href="index.html">HOME</a></li>
				<li><a href="index.html">ABOUT</a></li>
				<li><a href="index.html">FAQs</a></li>
				<li><a href="index.html">JOBS</a></li>
			</ul>
			<ul class="right">
				<li><a href="http://www.facebook.com/elia"><i class="fa fa-facebook"></i> </a></li>
				<li><a href="http://www.twitter.com/elia"><i class="fa fa-twitter"></i> </a></li>
				<li><a href="http://www.plus.google.com/"><i class="fa fa-google-plus"></i> </a></li>
				<li><a href="http://www.youtube.com/"><i class="fa fa-youtube"></i> </a></li>
				<li><a href="callto:"><i class="fa fa-phone"></i> +628 527 774 010 1</a></li>
				<li><a href="iighfar@gmail.com"><i class="fa fa-envelope"></i> cs@ecommerce.id</a></li>
			</ul>
		</div><!-- container -->
	</div><!-- navbar small -->
	<div class="header">
		<div class="container">
			<div class="nama">
			<h1> TOKO ONLINE </h1>
		
			</div>
			<ul class="right">
						<li><a href="category.html">CATEGORIES <i class="fa fa-angle-down"></i></a>
					<ul>

   <?php foreach($kategoris as $kategori) : 
       ?> 
						<li><a href="<?php echo base_url();?>index.php/home/categori/<?php echo $kategori['no_kategori'] ;?>"><?php echo $kategori['nama_kategori']; ?></a></li>
					<?php endforeach; ?>
																
					</ul>
				</li>
				<li class="cart"><a href="cart.html"><i class="fa fa-shopping-cart fa-lg"></i></a></li>
			</ul>
		</div><!-- container -->
	</div><!-- header -->
</body>
</html>

	