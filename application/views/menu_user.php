<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>



	<div class="header-small">
		<div class="container">
			<div class="left">
				<form action="<?php echo base_url();?>index.php/home/search_keyword" method="post">
					<input type="text" name="keyword" class="field" placeholder="Type your product ...">
					
					<button type="submit" class="submit"><i class="fa fa-send-o"></i> Search</button>
				</form>
				</div><!-- left -->
			<div class="right">
				<ul>
		
									  <li>
			<?php
				$text_cart_url  = '<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span>';
				$text_cart_url .= ' Inside Cart: '. $this->cart->total_items() .' items';
			?>
			<?=anchor('home/cart', $text_cart_url)?>
		</li>
		<?php if($this->session->userdata('nama_user')) { ?>
			<li><a href="<?php echo base_url();?>index.php/home">Halo <?=$this->session->userdata('nama_user')?> !</a></li>
			<li><?php echo anchor('login/logout', 'Logout');?></li>
		<?php } else { ?>
			<li><?php echo anchor('login', 'Login');?></li>
			<li><?php echo anchor('registrasi', 'Register');?></li>
		<?php } ?>
				</ul>
			</div><!-- right -->
		</div><!-- container -->
	</div><!-- header small -->
</body>
</html>

	