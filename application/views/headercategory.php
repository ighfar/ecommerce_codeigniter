<!DOCTYPE html>
<html>
<head>
	<title>eCommerce</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.transition.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/jquery-1.11.3.min.js"></script>
</head>
<body>



	<div class="header">
		<div class="container">
			<div class="logo">
				<img src="<?php echo base_url(); ?>elia/images/logo-ayolearn.png" />
			</div>
			<ul class="right">
						<li><a href="category.html">CATEGORIES <i class="fa fa-angle-down"></i></a>
					<ul>

   <?php foreach($kategoris as $kategori) : 
       ?> 
						<li><a href="<?php echo base_url();?>index.php/home/category/<?php echo $kategori['no_kategori'] ;?>"><?php echo $kategori['nama_kategori']; ?></a></li>
					<?php endforeach; ?>
																
					</ul>
				</li>
				<li class="cart"><a href="cart.html"><i class="fa fa-shopping-cart fa-lg"></i></a></li>
			</ul>
		</div><!-- container -->
	</div><!-- header -->


	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/main.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/wow.js"></script>


</body>
</html>