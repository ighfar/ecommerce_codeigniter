<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Front-End Toko Online by Kursus-PHP.com</title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	</head>
	<body>
	<?php $this->load->view('menu_user');?>
	<?php if($history != false) : ?>
			<?php $this->session->set_flashdata('pesan')?>
		
			<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th>Invoice Id #</th>
					<th>Invoice Date</th>
					<th>Due Date</th>
							<th>Jumlah Total</th>
					<th>Status</th>
										

									</tr>
			</thead>
			<tbody>
				<?php 
					foreach ($history as $row) : 
					?>
				<tr>
					<td><?= $row->id ?></td>
					<td><?= $row->date ?></td>
					<td><?= $row->due_date ?></td>
						<td><?= $row->total ?></td>
					<td><?= $row->status ?>
<a href="<?php echo base_url();?>index.php/costumer/payment_confirmation/<?php echo $row->id ; ?>" class="btn btn-primary btn-small">Konfirmasi</a>
					</td>
					
									</tr>
				<?php endforeach; ?>
			</tbody>
			
		</table>
	<?php else : ?>
		<p align="center">Tidak ada history belanja untukmu, <?= anchor('/','belanja sekarang')?></p>

	</body>
</html>