<!DOCTYPE html>
<html>

<head>
	<title>eCommerce</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.transition.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.transition.css">


	<script type="text/javascript">
	function cekform()
	{
		if(!${'#nama_user'}.val())
        {
        	alert("maaf, Username tidak boleh kosong");
        	${"#nama_user"}.focus();
        	return false;

	}
		if(!${'#password'}.val())
        {
        	alert("maaf, password tidak boleh kosong");
        	${"#password"}.focus();
        	return false;

	}
}
</script>
</head>
<body>

<?php $this->load->view('header') ?>
<?php $this->load->view('menu_user') ?>

	<div class="content-product">
		<div class="container">
			<h2 class="header-title">
				FORM LOGIN
			</h2>
			 <div><?php echo validation_errors(); ?></div>
			<form class="user" name="" method="POST" action="<?php echo base_url();?>index.php/login" onsubmit="return cekform();">
				<input type="text" name="nama_user" id="nama_user" placeholder="Username" />
				<input type="password" name="password" id="password" placeholder="Password" />
				<button type="submit" class="">Masuk</button>
				<a class="forget" href="#">Lupa password?</a>
			</form>
		</div><!-- container -->
	</div><!-- content product -->
	<div class="small-footer">
		<div class="container">
			<p class="copyright">
				All Right Reserved Ayolearn.com 2015
			</p>
			<p class="link">
				<a href="#">Top</a>
			</p>
		</div><!-- container -->
	</div><!-- small footer -->
		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script src="<?php echo base_url(); ?>elia/assets/js/jquery-2.1.4.min.js"></script>

		<!-- <![endif]-->

		<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo base_url(); ?>elia/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="<?php echo base_url(); ?>elia/assets/assets/js/bootstrap.min.js"></script>

		<!-- page specific plugin scripts -->
		<script src="<?php echo base_url(); ?>elia/assets/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>elia/assets/js/jquery.dataTables.bootstrap.min.js"></script>
		<script src="<?php echo base_url(); ?>elia/assets/js/dataTables.buttons.min.js"></script>
		<script src="<?php echo base_url(); ?>elia/assets/js/buttons.flash.min.js"></script>
		<script src="<?php echo base_url(); ?>elia/assets/js/buttons.html5.min.js"></script>
		<script src="<?php echo base_url(); ?>elia/assets/js/buttons.print.min.js"></script>
		<script src="<?php echo base_url(); ?>elia/assets/js/buttons.colVis.min.js"></script>
		<script src="<?php echo base_url(); ?>elia/assets/js/dataTables.select.min.js"></script>

	<script type="<?php echo base_url(); ?>elia/assets/text/javascript" src="js/jquery-1.11.3.min.js"></script>
	<script type="<?php echo base_url(); ?>elia/assets/text/javascript" src="js/main.js"></script>
	<script type="<?php echo base_url(); ?>elia/assets/text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="<?php echo base_url(); ?>elia/assets/text/javascript" src="js/wow.js"></script>
</body>
</html>