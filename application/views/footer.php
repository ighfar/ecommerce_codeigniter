<<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<div class="mega-footer">
		<div class="container">
			<div class="content">
				<h2>ABOUT US</h2>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat.
				</p>
			</div><!-- content -->
			<div class="content">
				<h2>SITEMAP</h2>
				<ul>
					<li><a href="#">Home</a></li>
					<li><a href="#">Downloads</a></li>
					<li><a href="#">Contact</a></li>
					<li><a href="#">Jobs</a></li>
				</ul>
			</div><!-- content -->
			<div class="content">
				<h2>GET IN TOUCH</h2>
				<p>Jl. Inkopad, Blok B11 No. 7, Bogor, Jawa Barat.</p>
				<p><i class="fa fa-phone"></i> (0251) 123456789</p>
				<p><i class="fa fa-envelope"></i> cs@ayolearn.com</p>
			</div><!-- content -->
		</div><!-- container -->
	</div><!-- mega footer -->

	<div class="small-footer">
		<div class="container">
			<p class="copyright">
				All Right Reserved Ayolearn.com 2015
			</p>
			<p class="link">
				<a href="#">Top</a>
			</p>
		</div><!-- container -->
	</div><!-- small footer -->
</body>
</html>

	