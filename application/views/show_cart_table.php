<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Front-End Toko Online by Kursus-PHP.com</title>
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		
			<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Nama Produk</th>
					<th>Jumlah</th>
					<th>Harga</th>
					<th>Subtotal</th>
					<th>Bayar Transaksi</th>
				</tr>
			</thead>
			<tbody>
				<?php 
					$i=0;
					foreach ($this->cart->contents() as $items) : 
					$i++;
				?>
				<tr>
					<td><?= $i ?></td>
					<td><?= $items['name'] ?></td>
					<td><?= $items['qty'] ?></td>
					<td align="right"><?= number_format($items['price'],0,',','.') ?></td>
					<td align="right"><?= number_format($items['subtotal'],0,',','.') ?></td>
					<td align="right">	<a href="<?php echo base_url();?>index.php/home/detail/<?php echo $items['id'] ;?>" class="btn btn-primary">Bayar Transaksi</a></td>
				</tr>
				<?php endforeach; ?>
			</tbody><tfoot>
				
			</tfoot>
		
		</table>
		<div align="center">
		<a href="<?php echo base_url();?>index.php/home/clear_cart" class="btn btn-danger">Bersihkan Keranjang</a>
		<a href="<?php echo base_url();?>index.php/admin/home" class="btn btn-primary">Belanja Lagi</a>
			<a href="<?php echo base_url();?>index.php/order" class="btn btn-success">Check Out</a>
		</div>
	</body>
</html>