<!DOCTYPE html>
<html>
<head>
	<title>eCommerce</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.transition.css">
</head>
<body>

	<div class="navbar-small">
		<div class="container">
			<ul class="left">
				<li><a href="index.html">HOME</a></li>
				<li><a href="index.html">ABOUT</a></li>
				<li><a href="index.html">FAQs</a></li>
				<li><a href="index.html">JOBS</a></li>
			</ul>
			<ul class="right">
				<li><a href="http://www.facebook.com/ayolearn"><i class="fa fa-facebook"></i> </a></li>
				<li><a href="http://www.twitter.com/ayolearn"><i class="fa fa-twitter"></i> </a></li>
				<li><a href="http://www.plus.google.com/+AnggaRiskySetiawan"><i class="fa fa-google-plus"></i> </a></li>
				<li><a href="http://www.youtube.com/anggariskysetiawan"><i class="fa fa-youtube"></i> </a></li>
				<li><a href="callto:"><i class="fa fa-phone"></i> +628 899 8501 870</a></li>
				<li><a href="mailto:anggariskysetiawan@gmail.com"><i class="fa fa-envelope"></i> cs@elia.id</a></li>
			</ul>
		</div><!-- container -->
	</div><!-- navbar small -->

	<div class="header">
		<div class="container">
			<div class="logo">
				<img src="images/logo-ayolearn.png" />
			</div>
			<ul class="right">
				<li><a href="category.html">MEN</a></li>
				<li><a href="category.html">WOMEN</a></li>
				<li><a href="category.html">ACCESSORIES</a></li>
				<li><a href="category.html">CATEGORIES <i class="fa fa-angle-down"></i></a>
					<ul>
						<li><a href="category.html">TREND</a></li>
						<li><a href="category.html">OLD</a></li>
						<li><a href="category.html">MUZZY</a></li>
						<li><a href="category.html">OTHERS</a></li>
					</ul>
				</li>
				<li class="cart"><a href="cart.html"><i class="fa fa-shopping-cart fa-lg"></i></a></li>
			</ul>
		</div><!-- container -->
	</div><!-- header -->

	<div class="header-small">
		<div class="container">
			<div class="left">
				<form action="result.html" method="">
					<input type="text" name="" class="field" placeholder="Type your product ...">
					<select option name="" class="select">
						<option value="">- Pilih Kategori -</option>
						<option value="">MEN</option>
						<option value="">WOMEN</option>
						<option value="">ACCESSORIES</option>
						<option value="">OTHERS</option>
					</select>
					<button type="submit" class="submit"><i class="fa fa-send-o"></i> Search</button>
				</form>
			</div><!-- left -->
			<div class="right">
				<ul>
					<li><a href="login.html">Login</a></li>
					<li class="register"><a href="register.html">Register</a></li>
				</ul>
			</div><!-- right -->
		</div><!-- container -->
	</div><!-- header small -->

	<div class="banner-slider">
		<div class="container">
			<div id="owl-banner-slider" class="owl-carousel owl-theme">
	     		<div class="item">
					<img src="<?php echo base_url(); ?>elia/images/banner1.jpg" alt="Discount Saat Ini">
				</div><!-- item -->
				<div class="item">
					<img src="<?php echo base_url(); ?>elia/images/banner2.jpg" alt="Discount Saat Ini">
				</div><!-- item -->
				<div class="item">
					<img src="<?php echo base_url(); ?>elia/images/banner3.jpg" alt="Discount Saat Ini">
				</div><!-- item -->
				<div class="item">
					<img src="<?php echo base_url(); ?>elia/images/banner2.jpg" alt="Discount Saat Ini">
				</div><!-- item -->
				<div class="item">
					<img src="<?php echo base_url(); ?>elia/images/banner1.jpg" alt="Discount Saat Ini">
				</div><!-- item -->
				<div class="item">
					<img src="<?php echo base_url(); ?>elia/images/banner3.jpg" alt="Discount Saat Ini">
				</div><!-- item -->
	    	</div><!-- owl banner slider -->
		</div><!-- container-->
	</div><!-- banner slider -->

	<div class="content-product">
		<div class="container">
			<h2 class="header-title">
				NEW ARRIVAL
			</h2>
			<div class="product">
				<img src="<?php echo base_url(); ?>elia/images/produk1.jpg" />
				<div class="text">
					<a href="detail.html"><h3>
					Package Macb ...
					</h3></a>
					<p>
						Rp. 280.000.000,-
					</p>
					<a href="cart.html" class="cart">
					<i class="fa fa-shopping-cart fa-lg"></i>
					</a>
				</div><!-- text -->
			</div><!-- product -->
			<div class="product">
				<img src="images/produk8.jpg" />
				<div class="text">
					<a href="detail.html"><h3>
					Package Macb ...
					</h3></a>
					<p>
						Rp. 280.000.000,-
					</p>
					<a href="cart.html" class="cart">
					<i class="fa fa-shopping-cart fa-lg"></i>
					</a>
				</div><!-- text -->
			</div><!-- product -->
			<div class="product">
				<img src="images/produk7.png" />
				<div class="text">
					<a href="detail.html"><h3>
					Package Macb ...
					</h3></a>
					<p>
						Rp. 280.000.000,-
					</p>
					<a href="cart.html" class="cart">
					<i class="fa fa-shopping-cart fa-lg"></i>
					</a>
				</div><!-- text -->
			</div><!-- product -->
			<div class="product">
				<img src="images/produk6.png" />
				<div class="text">
					<a href="detail.html"><h3>
					Package Macb ...
					</h3></a>
					<p>
						Rp. 280.000.000,-
					</p>
					<a href="cart.html" class="cart">
					<i class="fa fa-shopping-cart fa-lg"></i>
					</a>
				</div><!-- text -->
			</div><!-- product -->
			<div class="product">
				<img src="images/produk5.jpg" />
				<div class="text">
					<a href="detail.html"><h3>
					Package Macb ...
					</h3></a>
					<p>
						Rp. 280.000.000,-
					</p>
					<a href="cart.html" class="cart">
					<i class="fa fa-shopping-cart fa-lg"></i>
					</a>
				</div><!-- text -->
			</div><!-- product -->
			<div class="product">
				<img src="images/produk4.jpg" />
				<div class="text">
					<a href="detail.html"><h3>
					Package Macb ...
					</h3></a>
					<p>
						Rp. 280.000.000,-
					</p>
					<a href="cart.html" class="cart">
					<i class="fa fa-shopping-cart fa-lg"></i>
					</a>
				</div><!-- text -->
			</div><!-- product -->
			<div class="product">
				<img src="images/produk3.jpg" />
				<div class="text">
					<a href="detail.html"><h3>
					Package Macb ...
					</h3></a>
					<p>
						Rp. 280.000.000,-
					</p>
					<a href="cart.html" class="cart">
					<i class="fa fa-shopping-cart fa-lg"></i>
					</a>
				</div><!-- text -->
			</div><!-- product -->
			<div class="product">
				<img src="images/produk2.jpg" />
				<div class="text">
					<a href="detail.html"><h3>
					Package Macb ...
					</h3></a>
					<p>
						Rp. 280.000.000,-
					</p>
					<a href="cart.html" class="cart">
					<i class="fa fa-shopping-cart fa-lg"></i>
					</a>
				</div><!-- text -->
			</div><!-- product -->
		</div><!-- container -->
		<div class="indicator">
			<ul>
				<li><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">...</a></li>
				<li><a href="#">69</a></li>
			</ul>
		</div><!-- indicator -->
	</div><!-- content product -->

	<div class="testimonial">
		<div class="container">
			<h2 class="header-title">
				OUR TESTIMONIAL
			</h2>
			<div class="content">
				<img src="images/produk1.jpg" class="avatar" />
				<h3 class="nama">
					ayolearn.com
				</h3>
				<p class="text">
					" Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit "
				</p>
			</div><!-- content -->
			<div class="content">
				<img src="images/produk3.jpg" class="avatar" />
				<h3 class="nama">
					ayolearn-anggarisky.com
				</h3>
				<p class="text">
					" Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit "
				</p>
			</div><!-- content -->
		</div><!-- container -->
	</div><!-- testimonial -->

	<div class="mega-footer">
		<div class="container">
			<div class="content">
				<h2>ABOUT US</h2>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat.
				</p>
			</div><!-- content -->
			<div class="content">
				<h2>SITEMAP</h2>
				<ul>
					<li><a href="#">Home</a></li>
					<li><a href="#">Downloads</a></li>
					<li><a href="#">Contact</a></li>
					<li><a href="#">Jobs</a></li>
				</ul>
			</div><!-- content -->
			<div class="content">
				<h2>GET IN TOUCH</h2>
				<p>Jl. Inkopad, Blok B11 No. 7, Bogor, Jawa Barat.</p>
				<p><i class="fa fa-phone"></i> (0251) 123456789</p>
				<p><i class="fa fa-envelope"></i> cs@ayolearn.com</p>
			</div><!-- content -->
		</div><!-- container -->
	</div><!-- mega footer -->

	<div class="small-footer">
		<div class="container">
			<p class="copyright">
				All Right Reserved Ayolearn.com 2015
			</p>
			<p class="link">
				<a href="#">Top</a>
			</p>
		</div><!-- container -->
	</div><!-- small footer -->

	<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/wow.js"></script>
</body>
</html>