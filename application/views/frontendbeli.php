<!DOCTYPE html>
<html lang="en">
	<head>
		<title>eCommerce</title>

		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" />
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	</head>
	<body>
			
		<!-- Tampilkan semua produk -->
		<div class="row">
		<!-- looping products -->
		  <?php foreach($produks as $prd) : ?>
		  <div class="col-sm-3 col-md-3">
			<div class="thumbnail"><div class="product">
			 <img src="<?php echo base_url();?>elia/uploads/<?php echo $prd->gambar;?>" height="150" width="150"></div>
			  <div class="text">
				<a href="<?php echo base_url();?>index.php/admin/produk/detail/<?php echo $prd->id ;?>"><h3><?php echo $prd->nama_produk; ?></h3></a>
				<p><?php echo $prd->harga ;?></p>
				<p>
						<a href="<?php echo base_url();?>index.php/home/addcart/<?php echo $prd->id ;?>" class="cart">
					<i class="fa fa-shopping-cart fa-lg"></i>
					</a>
				</p>
			  </div>
			  
			</div>
		  </div>
		  <?php endforeach; ?>
		<!-- end looping -->
		</div>
		
	</body>
</html>