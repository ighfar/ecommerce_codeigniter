<!DOCTYPE html>
<html>
<head>
	<title>eCommerce</title>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.transition.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/jquery-1.11.3.min.js"></script>
</head>
<body>

	<div class="navbar-small">
		<div class="container">
			<ul class="left">
				<li><a href="index.html">HOME</a></li>
				<li><a href="index.html">ABOUT</a></li>
				<li><a href="index.html">FAQs</a></li>
				<li><a href="index.html">JOBS</a></li>
			</ul>
			<ul class="right">
				<li><a href="http://www.facebook.com/ayolearn"><i class="fa fa-facebook"></i> </a></li>
				<li><a href="http://www.twitter.com/ayolearn"><i class="fa fa-twitter"></i> </a></li>
				<li><a href="http://www.plus.google.com/+AnggaRiskySetiawan"><i class="fa fa-google-plus"></i> </a></li>
				<li><a href="http://www.youtube.com/anggariskysetiawan"><i class="fa fa-youtube"></i> </a></li>
				<li><a href="callto:"><i class="fa fa-phone"></i> +628 899 8501 870</a></li>
				<li><a href="mailto:anggariskysetiawan@gmail.com"><i class="fa fa-envelope"></i> cs@ecommerce.id</a></li>
			</ul>
		</div><!-- container -->
	</div><!-- navbar small -->

	<div class="header">
		<div class="container">
			<div class="logo">
				<img src="images/logo-ayolearn.png" />
			</div>
			<ul class="right">
				<li><a href="category.html">MEN</a></li>
				<li><a href="category.html">WOMEN</a></li>
				<li><a href="category.html">ACCESSORIES</a></li>
				<li><a href="category.html">CATEGORIES <i class="fa fa-angle-down"></i></a>
					<ul>
						<li><a href="category.html">TREND</a></li>
						<li><a href="category.html">OLD</a></li>
						<li><a href="category.html">MUZZY</a></li>
						<li><a href="category.html">OTHERS</a></li>
					</ul>
				</li>
				<li class="cart"><a href="cart.html"><i class="fa fa-shopping-cart fa-lg"></i></a></li>
			</ul>
		</div><!-- container -->
	</div><!-- header -->

	<div class="header-small">
		<div class="container">
			<div class="left">
				<form action="result.html">
					<input type="text" name="" class="field" placeholder="Type your product ...">
					<select option name="" class="select">
						<option value="">- Pilih Kategori -</option>
						<option value="">MEN</option>
						<option value="">WOMEN</option>
						<option value="">ACCESSORIES</option>
						<option value="">OTHERS</option>
					</select>
					<button type="submit" class="submit"><i class="fa fa-send-o"></i> Search</button>
				</form>
			</div><!-- left -->
			<div class="right">
				<ul>
					<li><a href="login.html">Login</a></li>
					<li class="register"><a href="register.html">Register</a></li>
				</ul>
			</div><!-- right -->
		</div><!-- container -->
	</div><!-- header small -->

	<div class="product-detail">
		<div class="container">
			<div class="left">
		<img src="<?php echo base_url();?>elia/uploads/<?php echo $hasil->gambar ;?>" height="100" width="100">
				<h3>
					Add to Action :
				</h3>
				<a href="cart.html">
					<div class="cart">
						<i class="fa fa-shopping-cart fa-lg"></i> Add to Cart
					</div>
				</a>
				<a href="cart.html">
					<div class="wishlist">
						<i class="fa fa-save fa-lg"></i> Add to Wishlist
					</div>
				</a>
				<a href="#">
					<div class="report">
						<i class="fa fa-close fa-lg"></i> Report this Product
					</div>
				</a>
				<a href="#">
					<div class="share">
						<i class="fa fa-envelope fa-lg"></i> Share this Product
					</div>
				</a>
			</div><!-- left -->
			<div class="right">
				<h2>
				<?php echo $hasil->nama_produk; ?>
				</h2>
				<h3 class="price">
					<?php echo $hasil->harga; ?>
				</h3>
				<h3>
					Deskripsi Produk :
				</h3>
				<p>
					<?php echo $hasil->keterangan; ?>
				</p>
				
				<h3>
					Reviews :
				</h3>
				<div class="content-review">
					<p class="rate">
						<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
						<i class="fa fa-star"></i> <i class="fa fa-star-half"></i>
					</p>
					<h4 class="buyer">
					
					</h4>
					<p class="commentar">
				
					</p>
				</div><!-- content review -->
				
				<h3>
					Beri Review :
				</h3>
				<div class="content-review">
					<form action="" method="">
						<input type="text" name="" required placeholder="Nama" />
						<input type="email" name="" required placeholder="Email Valid" />
						<textarea name="" required></textarea>
						<select required option name="">
							<option value="">- Rate This Product -</option>
							<option value="1">Bintang 1</option>
							<option value="2">Bintang 2</option>
							<option value="3">Bintang 3</option>
							<option value="4">Bintang 4</option>
							<option value="5">Bintang 5</option>
						</select>
						<button type="submit">
							<i class="fa fa-send-o"></i> Kirim
						</button>
					</form>
				</div><!-- content review -->
			</div><!-- right -->
		</div><!-- container -->
	</div><!-- product detail -->

	<div class="content-product">
		<div class="container">
			<h2 class="header-title">
				RELATED PRODUCT
			</h2>
				<?php $this->load->view('front_end'); ?>
	</div><!-- content product -->

	<div class="testimonial">
		<div class="container">
			<h2 class="header-title">
				OUR TESTIMONIAL
			</h2>
			<div class="content">
				<img src="images/produk1.jpg" class="avatar" />
				<h3 class="nama">
					ayolearn.com
				</h3>
				<p class="text">
					" Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit "
				</p>
			</div><!-- content -->
			<div class="content">
				<img src="images/produk3.jpg" class="avatar" />
				<h3 class="nama">
					ayolearn-anggarisky.com
				</h3>
				<p class="text">
					
				</p>
			</div><!-- content -->
		</div><!-- container -->
	</div><!-- testimonial -->

	<div class="mega-footer">
		<div class="container">
			<div class="content">
				<h2>ABOUT US</h2>
				<p>
					
				</p>
			</div><!-- content -->
			<div class="content">
				<h2>SITEMAP</h2>
				<ul>
					<li><a href="#">Home</a></li>
					<li><a href="#">Downloads</a></li>
					<li><a href="#">Contact</a></li>
					<li><a href="#">Jobs</a></li>
				</ul>
			</div><!-- content -->
			<div class="content">
				<h2>GET IN TOUCH</h2>
				<p>Jl. Inkopad, Blok B11 No. 7, Bogor, Jawa Barat.</p>
				<p><i class="fa fa-phone"></i> (0251) 123456789</p>
				<p><i class="fa fa-envelope"></i> cs@ayolearn.com</p>
			</div><!-- content -->
		</div><!-- container -->
	</div><!-- mega footer -->

	<div class="small-footer">
		<div class="container">
			<p class="copyright">
				All Right Reserved Ayolearn.com 2015
			</p>
			<p class="link">
				<a href="#">Top</a>
			</p>
		</div><!-- container -->
	</div><!-- small footer -->

	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/main.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/wow.js"></script>
</body>
</html>