<!DOCTYPE html>
<html>
<head>
	<title>eCommerce</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.transition.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/jquery-1.11.3.min.js"></script>
</head>
<body>

	<div class="navbar-small">
		<div class="container">
			<ul class="left">
					<li><a href="http://www.facebook.com/elia"><i class="fa fa-facebook"></i> </a></li>
				<li><a href="http://www.twitter.com/elia"><i class="fa fa-twitter"></i> </a></li>
				<li><a href="http://www.plus.google.com/+AnggaRiskySetiawan"><i class="fa fa-google-plus"></i> </a></li>
				<li><a href="http://www.youtube.com/anggariskysetiawan"><i class="fa fa-youtube"></i> </a></li>
				<li><a href="callto:"><i class="fa fa-phone"></i> +628 899 8501 870</a></li>
				<li><a href="iighfar@gmail.com"><i class="fa fa-envelope"></i> cs@ecommerce.id</a></li>
			</ul>
		</div><!-- container -->
	</div><!-- navbar small --><?php $this->load->view('header');?>

    <?php $this->load->view('menu_user');?>
	<div class="content-product">
		<div class="container">
		
		<p>Terima kasih, orderanmu sedang diproses..</p>
	</div><!-- content product -->

	

	<div class="small-footer">
		<div class="container">
			<p class="copyright">
				All Right Reserved Ayolearn.com 2015
			</p>
			<p class="link">
				<a href="#">Top</a>
			</p>
		</div><!-- container -->
	</div><!-- small footer -->

	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/main.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/wow.js"></script>


</body>
</html>