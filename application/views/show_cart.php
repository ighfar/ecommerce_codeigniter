<!DOCTYPE html>
<html>
<head>
	<title>eCommerce</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/main.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/animate.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.theme.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>elia/css/owl.transition.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/jquery-1.11.3.min.js"></script>
</head>
<body>

<?php $this->load->view('header');?>

    <?php $this->load->view('menu_user');?>
	<?php $this->load->view('show_cart_table'); ?>

	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/main.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>elia/js/wow.js"></script>


</body>
</html>