<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Costumer extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('model_costumer');
    }

  
	public function index()
	{
		}

	
			public function payment_confirmation($invoice_id)
	{
		$this->form_validation->set_rules('invoice_id', 'Invoice Id', 'required|integer');
		$this->form_validation->set_rules('jumlah', 'Jumlah Transfer', 'required|integer');
	
	
		  if ($this->form_validation->run() == FALSE)
		{
			if($this->input->post('invoice_id')){
					$data['invoice_id'] = set_value($invoice_id);
			}
			else{
					$data['invoice_id'] = $invoice_id;
			}
	$this->load->view('costumer/form_payment_confirmation', $data);
		}else{
			$isValid = $this->model_costumer->mark_invoice_confirmed(set_value('invoice_id'), set_value('jumlah'));

		if ($isValid) {
			$this->session->set_flashdata('pesan','Kami akan cek konfirmasi pembayaran anda');
			redirect('costumer/shopping_history');
		} else {
			$this->session->set_flashdata('error','Invoice Id dan Jumlah salah, silakan coba lagi !');
			redirect('costumer/payment_confirmation/'.set_value('invoice_id'));
			}
		}

		
	}
 
		public function shopping_history()
	{
	
		$user = $this->session->userdata('nama_user');
		$data['history'] = $this->model_costumer->get_shopping_history($user);
		$this->load->view('costumer/shopping_history_list', $data);
	}


	public function addcart($product_id)
	{
		$produks = $this->model_produk->find($product_id);
		$data = array(
					  'id'     => $produks->id,
					   'qty'  => 1,
					    'name'     => $produks->nama_produk,
					   'price'   => $produks->harga,
								);

		$this->cart->insert($data);
		redirect('home');
	}
	public function cart(){
		// displays what currently inside the cart
		//print_r($this->cart->contents());
		$this->load->view('show_cart');
	}

	public function clear_cart()
	{
		$this->cart->destroy();
		redirect('home');
	}
	
}