<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->model('model_produk');
    }

    public function search_keyword()
    {
        $keyword    =   $this->input->post('keyword');
        $data['results']    =   $this->model_produk->search($keyword);
        $this->load->view('produkcari',$data);
    }

	public function index()
	{
		$data = array(
		'produks' => $this->model_produk->getproduk()->result_array(), 

		);
		
		$data1  = array(
		'kategoris' => $this->model_produk->getkategori()->result_array(), 
		);
		$comp  = array(
		'frontend' => $this->load->view('frontend',$data,true), 
		'header' => $this->load->view('header',$data1,true), 
				);
		$this->load->view('home', $comp);
	}

	
			public function categori($id)
	{

		$data['kategoris'] = $this->model_produk->getkategori()->result_array();
		$data['produks'] = $this->model_produk->findget($id);
			
		$this->load->view('produkktgr', $data);
	}
 
		public function html_header()
	{
	
		$data  = array(
		'kategoris' => $this->model_produk->getkategori()->result_array(), 
		);
		$this->load->view('header',$data,true);
	}


	public function addcart($product_id)
	{
		$produks = $this->model_produk->find($product_id);
		$data = array(
					  'id'     => $produks->id,
					   'qty'  => 1,
					    'name'     => $produks->nama_produk,
					   'price'   => $produks->harga,
								);

		$this->cart->insert($data);
		redirect('home');
	}
	public function cart(){
		// displays what currently inside the cart
		//print_r($this->cart->contents());
		$this->load->view('show_cart');
	}

	public function detail($id){
		$data['hasil'] = $this->model_produk->find($id);
		$this->load->view('pembelian',$data);
	}

	public function clear_cart()
	{
		$this->cart->destroy();
		redirect('home');
	}
	
}