<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Detail extends CI_Controller {

	public function index()
	{
		$data['produk'] = $this->model_produk->all();
		$this->load->view('detail', $data);
	}
}