<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produk extends CI_Controller {
	
	  public function __construct() {
        parent::__construct();
        $this->load->model('model_produk'); //load model mupload yang berada di folder model
        $this->load->helper(array('url')); //load helper url 

    }
	public function index()
	{
		$data  = array(
		'kategoris' => $this->model_produk->getkategori()->result_array(), 
		);
		$data['produk'] = $this->model_produk->all();
		$this->load->view('produk', $data);
	}
	
	public function tambah()
	{
		$this->load->view('tambah_produk');
	}
	public function simpan(){
		//form validation sebelum mengeksekusi QUERY INSERT
		$this->form_validation->set_rules('no_kategori', 'No. Kategori', 'required');
		$this->form_validation->set_rules('nama_produk', 'Nama Produk', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
			$this->form_validation->set_rules('harga', 'Harga', 'required');
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'required|integer');
		//$this->form_validation->set_rules('userfile', 'Product Image', 'required');

		$this->load->library('upload');
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './elia/uploads/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '1288'; //lebar maksimum 1288 px
        $config['max_height']  = '768'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya


        $this->upload->initialize($config);
        if ($this->form_validation->run() == FALSE)
		{

			$this->load->view('tambah_produk');
		}else{


		if ($_FILES['userfile']['name'])
			  	
        {
        	 if ($this->upload->do_upload('userfile'))
            {
            	
                $gbr = $this->upload->data();
                $data = array(
                'no_kategori' =>$this->input->post('no_kategori'),
                'nama_produk' =>$this->input->post('nama_produk'),
                'keterangan' =>$this->input->post('keterangan'),
                'harga' =>$this->input->post('harga'),
                'gambar' =>$gbr['file_name'],
                'jumlah' =>$this->input->post('jumlah'),
                  
                ); $this->model_produk->get_insert($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">input Data Berhasil !!</div></div>");
                redirect('admin/produk'); //jika berhasil maka akan ditampilkan view vupload
            }else{
                //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Input Data Gagal !!</div></div>");
                redirect('admin/produk/tambah'); //jika gagal maka akan ditampilkan form upload
            }
        }
    }
}
	public function edit($id){
		$data['hasil'] = $this->model_produk->find($id);
		$this->load->view('edit_produk',$data);
	}
    public function fgt($id){
        $data['hasil'] = $this->model_produk->findget($id);
        $this->load->view('home',$data);
    }
	public function detail($id){
		$data['hasil'] = $this->model_produk->find($id);
		$this->load->view('detail',$data);
	}
	public function update($id){

		$this->load->library('upload');
        $nmfile = "file_".time(); //nama file saya beri nama langsung dan diikuti fungsi time
        $config['upload_path'] = './elia/uploads/'; //path folder
        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp'; //type yang dapat diakses bisa anda sesuaikan
        $config['max_size'] = '2048'; //maksimum besar file 2M
        $config['max_width']  = '1288'; //lebar maksimum 1288 px
        $config['max_height']  = '768'; //tinggi maksimu 768 px
        $config['file_name'] = $nmfile; //nama yang terupload nantinya


        $this->upload->initialize($config);
      
		if (!empty($_FILES['userfile']['name']['0']))
			  	
        {
        	 $this->upload->do_upload();
                $gbr = array('upload_data' => $this->upload->data());
                $data = array(
                'no_kategori' =>$this->input->post('no_kategori'),
                'nama_produk' =>$this->input->post('nama_produk'),
                'keterangan' =>$this->input->post('keterangan'),
                'harga' =>$this->input->post('harga'),
                'gambar' =>$gbr['upload_data']['file_name'],
                'jumlah' =>$this->input->post('jumlah'),
                  
                ); $this->model_produk->update($id,$data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Update data berhasil !!</div></div>");
                redirect('admin/produk'); //jika berhasil maka akan ditampilkan view vupload
            }elseif (empty($_FILES['userfile']['name']['0'])){
            	    $data = array(
                'no_kategori' =>$this->input->post('no_kategori'),
                'nama_produk' =>$this->input->post('nama_produk'),
                'keterangan' =>$this->input->post('keterangan'),
                'harga' =>$this->input->post('harga'),
                'jumlah' =>$this->input->post('jumlah'),
                  
                ); $this->model_produk->update($id,$data);
                       //pesan yang muncul jika terdapat error dimasukkan pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Update data berhasil!!</div></div>");
                redirect('admin/produk/'); //jika gagal maka akan ditampilkan form upload
            }
        }
    
	
	public function delete($id){
		$this->model_produk->delete($id);
		redirect('admin/produk');
	}
}