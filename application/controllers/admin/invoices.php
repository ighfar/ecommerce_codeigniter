<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoices extends CI_Controller {
	public function __construct(){
		parent::__construct();
		
	
		
		//load model -> model_products
		$this->load->model('model_orders');
	}
	
	public function index()
	{
			$data  = array(
		'kategoris' => $this->model_produk->getkategori()->result_array(), 
		);
		$data['invoices'] = $this->model_orders->all();
		$this->load->view('invoices', $data);
	}

    public function detail($invoice_id)
    {
        $data['invoice'] = $this->model_orders->get_invoice_by_id($invoice_id);
        $data['orders']  = $this->model_orders->get_orders_by_invoice($invoice_id);
		$this->load->view('invoices_detail', $data);
    }
}
