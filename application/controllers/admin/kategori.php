<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kategori extends CI_Controller {
	
	  public function __construct() {
        parent::__construct();
        $this->load->model('model_kategori'); //load model mupload yang berada di folder model
        $this->load->helper(array('url')); //load helper url 

    }
	public function index()
	{
				$data  = array(
		'kategoris' => $this->model_produk->getkategori()->result_array(), 
		);
		$data['kategori'] = $this->model_kategori->all();
		$this->load->view('kategori', $data);
	}
	
	public function tambah()
	{
		$this->load->view('tambah_kategori');
	}
	public function simpan(){
		//form validation sebelum mengeksekusi QUERY INSERT
		$this->form_validation->set_rules('no_kategori', 'No. Kategori', 'required|integer');
		$this->form_validation->set_rules('nama_kategori', 'Nama Kategori', 'required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
				//$this->form_validation->set_rules('userfile', 'Product Image', 'required');

        if ($this->form_validation->run() == FALSE)
		{

			$this->load->view('tambah_kategori');
			 $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Input Data Gagal !!</div></div>");
                redirect('admin/kategori/tambah'); //jika gagal maka akan ditampilkan form upload
		}else{
          	
               
                $data = array(
                'no_kategori' =>$this->input->post('no_kategori'),
                'nama_kategori' =>$this->input->post('nama_kategori'),
                'keterangan' =>$this->input->post('keterangan'),
                                               
                ); $this->model_kategori->get_insert($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">input Data Berhasil !!</div></div>");
                redirect('admin/kategori'); //jika berhasil maka akan ditampilkan view vupload
           
        }
    }

	public function edit($no_kategori){
		$data['hasil'] = $this->model_kategori->find($no_kategori);
		$this->load->view('edit_kategori',$data);
	}

	public function detail($no_kategori){
		$data['hasil'] = $this->model_kategori->find($no_kategori);
		$this->load->view('detail',$data);
	}
	public function update($no_kategori){

	
                $data = array(
               'no_kategori' =>$this->input->post('no_kategori'),
                'nama_kategori' =>$this->input->post('nama_kategori'),
                'keterangan' =>$this->input->post('keterangan'),
                                
                ); $this->model_produk->update($no_kategori,$data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Update data berhasil !!</div></div>");
                redirect('admin/kategori'); //jika berhasil maka akan ditampilkan view vupload
          
        }
    public function get($nama_kategori){
		$data['hasil'] = $this->model_kategori->findget($nama_kategori);
		$this->load->view('home',$data);
	}
	
	public function delete($no_kategori){
		$this->model_kategori->delete($no_kategori);
		redirect('admin/kategori');
	}
}