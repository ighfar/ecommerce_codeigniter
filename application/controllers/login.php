<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{

		$data  = array(
		'kategoris' => $this->model_produk->getkategori()->result_array(), 
		);
		$this->load->view('login', $data);
		$this->form_validation->set_rules('nama_user','Username','required|alpha_numeric');
		$this->form_validation->set_rules('password','Password','required|alpha_numeric');
		
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('login');
		} else {
			$this->load->model('model_login');
			$valid_user = $this->model_login->check_credential();
			
			if($valid_user == FALSE)
			{
				$this->session->set_flashdata('error','Username / Password anda salah!');
				redirect('login');
			} else {
				// if the username and password is a match
				$this->session->set_userdata('nama_user', $valid_user->nama_user);
				$this->session->set_userdata('status', $valid_user->status);
				$this->session->set_userdata('alamat', $valid_user->alamat);

				switch($valid_user->status){
					case 1 : //admin
								redirect('admin/produk'); 
								break;
					case 2 : //member
								redirect('home');
								break;
					default: break; 
				}
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
}