<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Registrasi extends CI_Controller {
	
	  public function __construct() {
        parent::__construct();
        $this->load->model('model_register'); //load model mupload yang berada di folder model
        $this->load->helper(array('url')); //load helper url 

    }
	public function index()
	{
		$this->load->view('register');
	}
	
	
	public function simpan(){
		//form validation sebelum mengeksekusi QUERY INSERT
		$this->form_validation->set_rules('no_hp', 'No. HP', 'required');
		$this->form_validation->set_rules('nama_user', 'Nama User', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
				//$this->form_validation->set_rules('userfile', 'Product Image', 'required');

        if ($this->form_validation->run() == FALSE)
		{

			$this->load->view('register');
			 $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-danger\" id=\"alert\">Registrasi Gagal, User telah terdaftar !!</div></div>");
                redirect('registrasi'); //jika gagal maka akan ditampilkan form upload
		}else{
          	
               
                $data = array(
                'no_hp' =>$this->input->post('no_hp'),
                'nama_user' =>$this->input->post('nama_user'),
                'password' =>$this->input->post('password'),
                 'alamat' =>$this->input->post('alamat'),
                'email' =>$this->input->post('email'),
                'status' => '2',
                                  
                ); $this->model_register->get_insert($data); //akses model untuk menyimpan ke database
                //pesan yang muncul jika berhasil diupload pada session flashdata
                $this->session->set_flashdata("pesan", "<div class=\"col-md-12\"><div class=\"alert alert-success\" id=\"alert\">Registrasi Berhasil !!</div></div>");
                redirect('login'); //jika berhasil maka akan ditampilkan view vupload
           
        }
    }
}